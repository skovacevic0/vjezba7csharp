﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nesto {
    interface IRacun {
        decimal DohvatiIznos();
        DateTime DohvatiDatumIzdavanja();
    }
}
