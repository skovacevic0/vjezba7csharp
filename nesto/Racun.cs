﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nesto {
    class Racun<T> : IRacun {
        private DateTime datum;
        private T iznosRacuna;
        public Racun(T iznosRacuna) 
        {
            Datum = DateTime.Now;
            IznosRacuna = iznosRacuna;
        }

        
        public DateTime Datum { get; set; }
        public T IznosRacuna { get; set; }

        public DateTime DohvatiDatumIzdavanja() {
            return datum;
        }

        public decimal DohvatiIznos() {
            return Convert.ToDecimal(iznosRacuna);
        }
    }
}

