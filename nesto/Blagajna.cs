﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nesto {
    class Blagajna {
        List<IRacun> lista = new List<IRacun>();

        public void DodajRacun(IRacun racun) {
            lista.Add(racun);
        }
        void PrikaziRacun() {
            foreach (var racun in lista) {
                Console.WriteLine(racun.DohvatiIznos());

            }
        }
    }
}
